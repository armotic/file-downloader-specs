package fr.armotic.bamboo.plugins.filedownloader.specs;

import com.atlassian.bamboo.specs.api.BambooSpec;
import com.atlassian.bamboo.specs.api.builders.AtlassianModule;
import com.atlassian.bamboo.specs.api.builders.BambooKey;
import com.atlassian.bamboo.specs.api.builders.permission.Permissions;
import com.atlassian.bamboo.specs.api.builders.permission.PermissionType;
import com.atlassian.bamboo.specs.api.builders.permission.PlanPermissions;
import com.atlassian.bamboo.specs.api.builders.plan.artifact.Artifact;
import com.atlassian.bamboo.specs.api.builders.plan.branches.BranchCleanup;
import com.atlassian.bamboo.specs.api.builders.plan.branches.PlanBranchManagement;
import com.atlassian.bamboo.specs.api.builders.plan.configuration.ConcurrentBuilds;
import com.atlassian.bamboo.specs.api.builders.plan.Job;
import com.atlassian.bamboo.specs.api.builders.plan.Plan;
import com.atlassian.bamboo.specs.api.builders.plan.PlanIdentifier;
import com.atlassian.bamboo.specs.api.builders.plan.Stage;
import com.atlassian.bamboo.specs.api.builders.project.Project;
import com.atlassian.bamboo.specs.api.builders.task.AnyTask;
import com.atlassian.bamboo.specs.builders.task.CommandTask;
import com.atlassian.bamboo.specs.util.BambooServer;
import com.atlassian.bamboo.specs.util.MapBuilder;

/**
 * Plan configuration for Bamboo.
 *
 * @see <a href="https://confluence.atlassian.com/display/BAMBOO/Bamboo+Specs">Bamboo Specs</a>
 */
@BambooSpec
public class PlanSpec {

    /**
     * Run 'main' to publish your plan.
     */
    public static void main(String[] args) {
        // by default credentials are read from the '.credentials' file
        BambooServer bambooServer = new BambooServer("http://localhost:6990/bamboo");

        Plan plan = new PlanSpec().createPlan();
        bambooServer.publish(plan);

        PlanPermissions planPermission = new PlanSpec().createPlanPermission(plan.getIdentifier());
        bambooServer.publish(planPermission);
    }

    PlanPermissions createPlanPermission(PlanIdentifier planIdentifier) {
        Permissions permissions = new Permissions()
                .userPermissions("admin", PermissionType.ADMIN)
                .groupPermissions("bamboo-admin", PermissionType.ADMIN)
                .loggedInUserPermissions(PermissionType.BUILD)
                .anonymousUserPermissionView();

        return new PlanPermissions(planIdentifier)
                .permissions(permissions);
    }

    Project project() {
        return new Project()
                .name("My Project")
                .key("PROJ");
    }

    Plan createPlan() {
        Project project = new Project()
                .key(new BambooKey("FD"))
                .name("file-downloader");
        final Plan plan = new Plan(project,
            "FD - Installed plugin",
            new BambooKey("FIP"))
            .description("Test File Downloader as an installed plugin")
            .pluginConfigurations(new ConcurrentBuilds()
                    .useSystemWideDefault(false)
                    .maximumNumberOfConcurrentBuilds(3))
            .stages(new Stage("Test installed plugin")
                    .jobs(new Job("Test installed plugin",
                            new BambooKey("TIP"))
                            .artifacts(new Artifact()
                                    .name("README.md")
                                    .copyPattern("README.md")
                                    .shared(true))
                            .tasks(new AnyTask(new AtlassianModule("fr.armotic.bamboo.plugins.filedownloader:fd_task"))
                                    .description("Download and Extract Maven")
                                    .configuration(new MapBuilder()
                                            .put("filetype", "ARCHIVE")
                                            .put("variablepattern", "NONE")
                                            .put("lineending", "LF")
                                            .put("onlinetest", "true")
                                            .put("archiveextraction", "true")
                                            .put("fileorigin", "")
                                            .put("authenticationtype", "NONE")
                                            .put("login", "")
                                            .put("repository", "")
                                            .put("password", "")
                                            .put("filedestination", "maven")
                                            .put("locationtype", "ONLINE")
                                            .put("onlinelocation", "http://apache.mirrors.ovh.net/ftp.apache.org/dist/maven/maven-3/3.3.9/binaries/apache-maven-3.3.9-bin.tar.gz")
                                            .put("inlinebody", "")
                                            .build()),
                                new AnyTask(new AtlassianModule("fr.armotic.bamboo.plugins.filedownloader:fd_task"))
                                    .description("Download protected file from Nexus")
                                    .configuration(new MapBuilder()
                                            .put("filetype", "TEXT")
                                            .put("variablepattern", "NONE")
                                            .put("lineending", "LF")
                                            .put("onlinetest", "")
                                            .put("archiveextraction", "false")
                                            .put("fileorigin", "")
                                            .put("authenticationtype", "BASIC")
                                            .put("login", "fd-tester")
                                            .put("repository", "")
                                            .put("password", "eibP2faGpXHLxciDLtWc")
                                            .put("filedestination", "README-1.0.0.md")
                                            .put("locationtype", "ONLINE")
                                            .put("onlinelocation", "http://nexus.armotic.fr/repository/it/com/atlassian/bamboo/test/README/1.0.0/README-1.0.0.md")
                                            .put("inlinebody", "")
                                            .build()),
                                new AnyTask(new AtlassianModule("fr.armotic.bamboo.plugins.filedownloader:fd_task"))
                                    .description("Download test.png from FTP")
                                    .configuration(new MapBuilder()
                                            .put("filetype", "ARCHIVE")
                                            .put("variablepattern", "NONE")
                                            .put("lineending", "LF")
                                            .put("onlinetest", "")
                                            .put("archiveextraction", "false")
                                            .put("fileorigin", "")
                                            .put("authenticationtype", "BASIC")
                                            .put("login", "demo")
                                            .put("repository", "")
                                            .put("password", "password")
                                            .put("filedestination", "test.png")
                                            .put("locationtype", "ONLINE")
                                            .put("onlinelocation", "ftp://test.rebex.net/pub/example/winceclient.png")
                                            .put("inlinebody", "")
                                            .build()),
                                new AnyTask(new AtlassianModule("fr.armotic.bamboo.plugins.filedownloader:fd_task"))
                                    .description("Download README.md from repository")
                                    .configuration(new MapBuilder()
                                            .put("filetype", "TEXT")
                                            .put("variablepattern", "NONE")
                                            .put("lineending", "LF")
                                            .put("onlinetest", "")
                                            .put("archiveextraction", "false")
                                            .put("fileorigin", "README.md")
                                            .put("authenticationtype", "NONE")
                                            .put("login", "")
                                            .put("repository", "3178498:com.atlassian.bamboo.plugins.atlassian-bamboo-plugin-bitbucket:bbCloud")
                                            .put("password", "")
                                            .put("filedestination", "README.md")
                                            .put("locationtype", "REPOSITORY")
                                            .put("onlinelocation", "")
                                            .put("inlinebody", "")
                                            .build()),
                                new AnyTask(new AtlassianModule("fr.armotic.bamboo.plugins.filedownloader:fd_task"))
                                    .description("Download inline script")
                                    .configuration(new MapBuilder()
                                            .put("filetype", "SCRIPT")
                                            .put("variablepattern", "{{VAR}}")
                                            .put("lineending", "LF")
                                            .put("onlinetest", "")
                                            .put("archiveextraction", "false")
                                            .put("fileorigin", "")
                                            .put("authenticationtype", "NONE")
                                            .put("login", "")
                                            .put("repository", "")
                                            .put("password", "")
                                            .put("filedestination", "check.py")
                                            .put("locationtype", "INLINE")
                                            .put("onlinelocation", "")
                                            .put("inlinebody", "#!python3\nimport os.path\n\nif not os.path.isfile('maven/apache-maven-3.3.9/bin/mvn'):\n    raise Exception(\"mvn not found\")\n\nif not os.path.isfile('maven/apache-maven-3.3.9/conf/settings.xml'):\n    raise Exception(\"settings.xml not found\")\n\nif not os.path.isfile('README-1.0.0.md'):\n    raise Exception(\"protected file not found\")\n\nif not os.path.isfile('test.png'):\n    raise Exception(\"test.png not found\")\n\nif not os.path.isfile('README.md'):\n    raise Exception(\"README.md not found\")\n\nif not \"<fd-var:bamboo.buildKey>\".startswith('$'):\n    raise Exception(\"Variable should have not been replaced: <fd-var:bamboo.buildKey>\")\n    \nif not \"{{bamboo.buildPlanName}}\" == 'file-downloader - FD - Installed plugin - Test installed plugin':\n    raise Exception(\"Variable should have been replaced: {{bamboo.buildPlanName}}\")")
                                            .build()),
                                new CommandTask()
                                    .description("Check result")
                                    .executable("Python 3")
                                    .argument("check.py"))))
            .linkedRepositories("File Downloader")
            
            .planBranchManagement(new PlanBranchManagement()
                    .delete(new BranchCleanup())
                    .notificationForCommitters());
        return plan;
    }
}
