MVN ?= mvn

help::
	@grep '^[^#[:space:]]*::\?.*#.*' Makefile | GREP_COLOR='01;34' grep --color=always '^[^:]*' | GREP_COLOR='01;36' grep --color '[^#]*'


test:: # Performs offline validation of the plan
	${MVN} test

publish:: # Upload the plan to your Bamboo server
	${MVN} -Ppublish-specs
