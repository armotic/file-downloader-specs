File Downloader Specs
=====================
    Contributors: 'brice.ruppen@armotic.fr'
    License: Apache License 2.0
    License URI: https://bitbucket.org/armotic/file-downloader/src/master/LICENSE

This in the specs used to test the file-downloader

1. Download and Extract Maven  
This test verifies the download over http and the extractrion of a tar.gz.


2. Download protected file from Nexus  
This one is used to test a download over https with basic authentication.


3. Download 512KB.zip from FTP  
Tests a non authenticated download from FTP.


4. Download 1KB.zip from FTP  
Tests an authenticated download from FTP.


5. Download README.md from repository  
Verifies the possibility to get a file from a repository.


6. Download inline script  
Copies a file, written directly in the specs, to the workspace.


7. Check result  
Checks everything was downloaded correctly.